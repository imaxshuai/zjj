new Vue({
    el: '#app',

    data: {
        news: [],
        message: 'Hello Vue'
    },

    mounted(){
        this.getNews();
    },

    methods: {
        getNews(){
            var that = this;
            axios.get('http://localhost:63342/zjj/avision/news.json')
                .then((res)=>{
                    console.log(res);
                    this.news = res.data.data
                })
                .catch(err=>{
                    throw  err
                })
        },
        moreNews(){
            axios.get('http://localhost:63342/zjj/avision/news.json')
                .then((res)=>{
                    console.log(res);
                    this.news = this.news.concat(res.data.data)
                })
                .catch(err=>{
                    throw  err
                })
        }
    }

});